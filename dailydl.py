#coding: utf-8
import requests
import re
import wget
import os

qualites={"240p":"stream_h264_ld_url","360p":"stream_h264_url","480p":"stream_h264_hq_url","720p":"stream_h264_hd_url","1080p":"stream_h264_hd1080_url"}

lien = input("URL de la vidéo : ")

# on extrait l'id
id=re.search(r'o[/][a-z0-9]*',lien).group(0).replace("o/","")

# on recupère json qui liste les différentes qualités de la vidéo
url = "http://www.dailymotion.com/json/video/"+id+"?fields=title,stream_h264_url,stream_h264_ld_url,stream_h264_hq_url,stream_h264_hd_url,stream_h264_hd1080_url"
r=requests.get(url)

# on demande la qualité voulue (et on vérifie sa disponibilité)
dispos=""
for q in qualites:
	if r.json()[qualites[q]] != None:
		dispos+=" "+q
print(dispos)
qual=input("Qualité : ")
video = r.json()[qualites[qual]]

# on récupère la vidéo dans la qualité voulue
fichier = wget.download(video.replace("\\",""))

# on renomme enfin
nom=r.json()['title']+'.mp4'
os.rename(fichier,nom)
print()
print(nom)
